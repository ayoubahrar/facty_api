package com.facty.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FactyApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(FactyApiApplication.class, args);
	}

}
