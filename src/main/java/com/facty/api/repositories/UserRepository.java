package com.facty.api.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.facty.api.domain.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
	
	public List<User> findAll();
	
	public Optional<User> findByUsername(String username);
	
	public Optional<User> findByUsernameAndPassword(String username, String password);
}
