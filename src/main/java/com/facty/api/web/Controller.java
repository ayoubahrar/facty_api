package com.facty.api.web;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {

	@GetMapping("")
	public String sayHi() {
		return "Hi.";
	}
	
	@RequestMapping(method=RequestMethod.POST,value="/error")
	public String error() {
		return "error.";
	}
	
	@GetMapping("/authentification")
	public Authentication defaultUser1() {
		return SecurityContextHolder.getContext().getAuthentication();
	}
	
	@GetMapping("/context")
	public Object defaultUser2() {
		return SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}
}
