package com.facty.api.services;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class FactyUserDetailService implements UserDetailsService{

	@Autowired
	UserService userService;
	
	@Override
	public UserDetails loadUserByUsername(String username) {
		com.facty.api.domain.User user = userService.getUser(username);
		if(user!=null) {
			return new User(user.getUsername(),
					user.getPassword(),
					new ArrayList<>());
		}
		 throw new UsernameNotFoundException(username);
	}
	
	
	
}
