package com.facty.api.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.facty.api.domain.User;
import com.facty.api.repositories.UserRepository;
import com.facty.api.services.UserService;


@Service
public class UserServiceImpl implements UserService{

	Logger log = LoggerFactory.getLogger(UserService.class);
	
	@Autowired
	UserRepository userRepo;
	
	@Override
	public List<User> getAllUsers() {
		if(userRepo.findAll()==null) {
			log.info("Cannot get users from database");
			return new ArrayList<User>();
		}else {
			return userRepo.findAll();
		}
	}

	@Override
	public User getUser(String username) {
		if(userRepo.findByUsername(username).isPresent()) {
			return userRepo.findByUsername(username).get();
		}else{
			log.info("Cannot get user with username : " +username );
			return null;
		}
	}
	
	@Override
	public User getUser(String username, String password) {
		if(userRepo.findByUsernameAndPassword(username, password)!=null) {
			return userRepo.findByUsernameAndPassword(username, password).get();
		}else{
			log.info("Cannot get user with username : " +username +" password : "+password);
			return null;
		}
	}


	
	
}
