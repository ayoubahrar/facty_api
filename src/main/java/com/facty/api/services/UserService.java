package com.facty.api.services;

import java.util.List;

import org.springframework.stereotype.Service;

import com.facty.api.domain.User;

@Service
public interface UserService {
	
	public List<User> getAllUsers();
	
	public User getUser(String username);
	
	public User getUser(String username, String password);
}
